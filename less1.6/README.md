http://35.228.190.238:8001  
http://35.228.190.238:8002  
При добавлении healthcheck в grafana\Dockerfile  
контейнер с графаной стал помечаться статусом healthy\unhealthy  
но при этом оставался в запущеном состоянии.
```
[dkhromenko@instance-1 grafana]$ sudo docker ps
CONTAINER ID   IMAGE        COMMAND                  CREATED          STATUS                   PORTS                                               NAMES
47a832596928   grafana:v1   "/run.sh"                2 minutes ago    Up 2 minutes (healthy)   3000/tcp                                            grafana
b95cc4733592   nginx1:v1    "/docker-entrypoint.…"   27 minutes ago   Up 27 minutes            80/tcp, 0.0.0.0:8001->8001/tcp, :::8001->8001/tcp   nginx1
3d88423dc40f   nginx2:v1    "/docker-entrypoint.…"   27 minutes ago   Up 27 minutes            80/tcp, 0.0.0.0:8002->8002/tcp, :::8002->8002/tcp   nginx2
[dkhromenko@instance-1 grafana]$
[dkhromenko@instance-1 grafana]$
[dkhromenko@instance-1 grafana]$ sudo docker rm -f nginx1
nginx1
[dkhromenko@instance-1 grafana]$
[dkhromenko@instance-1 grafana]$
[dkhromenko@instance-1 grafana]$ sudo docker ps
CONTAINER ID   IMAGE        COMMAND                  CREATED          STATUS                   PORTS                                               NAMES
47a832596928   grafana:v1   "/run.sh"                2 minutes ago    Up 2 minutes (healthy)   3000/tcp                                            grafana
3d88423dc40f   nginx2:v1    "/docker-entrypoint.…"   28 minutes ago   Up 28 minutes            80/tcp, 0.0.0.0:8002->8002/tcp, :::8002->8002/tcp   nginx2
[dkhromenko@instance-1 grafana]$
[dkhromenko@instance-1 grafana]$
[dkhromenko@instance-1 grafana]$ sudo docker ps
CONTAINER ID   IMAGE        COMMAND                  CREATED          STATUS                     PORTS                                               NAMES
47a832596928   grafana:v1   "/run.sh"                3 minutes ago    Up 3 minutes (unhealthy)   3000/tcp                                            grafana
3d88423dc40f   nginx2:v1    "/docker-entrypoint.…"   29 minutes ago   Up 29 minutes              80/tcp, 0.0.0.0:8002->8002/tcp, :::8002->8002/tcp   nginx2
[dkhromenko@instance-1 grafana]$
[dkhromenko@instance-1 grafana]$
```
