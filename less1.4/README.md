```
docker build -t nginx-lua:v1 -f Dockerfile .
docker run --publish 8111:80 --volume $(pwd)/nginx.conf:/usr/local/nginx/conf/nginx.conf --name nginx-lua --detach nginx-lua:v1
curl http://localhost:8111/lua
```
